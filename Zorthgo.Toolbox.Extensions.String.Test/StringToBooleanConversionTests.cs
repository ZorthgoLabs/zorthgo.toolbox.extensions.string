﻿using Xunit;

namespace Zorthgo.Toolbox.Extensions.String.Test
{
    public class StringToBooleanConversionTests
    {
        [Fact]
        public void Bool_One_Should_Convert_To_True()
        {
            //-------------------------------------------------------------------------------------
            // NOTES: Testing whether the string to boolean converter can handle converting "1" to 
            //        true.
            //-------------------------------------------------------------------------------------

            // ARRANGE
            var value = "1";

            // ACT
            var isCastSuccess = value.TryConvertTo(out bool parsedValue);

            // ASSERT
            Assert.True(isCastSuccess, "The isCastSuccess returned [false] when the expected value was [true].");
            Assert.True(parsedValue);
        }

        [Fact]
        public void Bool_True_Should_Convert_To_True()
        {
            //-------------------------------------------------------------------------------------
            // NOTES: Testing whether the string to boolean converter can handle converting the
            //        string value "True" to it's boolean counterpart.
            //-------------------------------------------------------------------------------------

            // ARRANGE
            var value = "True";

            // ACT
            var isCastSuccess = value.TryConvertTo(out bool parsedValue);

            // ASSERT
            Assert.True(isCastSuccess, "The isCastSuccess returned [false] when the expected value was [true].");
            Assert.True(parsedValue);
        }

        [Fact]
        public void Bool_Zero_Should_Convert_To_False()
        {
            //-------------------------------------------------------------------------------------
            // NOTES: Testing whether the string to boolean converter can handle converting "0" to 
            //        false.
            //-------------------------------------------------------------------------------------

            // ARRANGE
            var value = "0";

            // ACT
            var isCastSuccess = value.TryConvertTo(out bool parsedValue);

            // ASSERT
            Assert.True(isCastSuccess, "The isCastSuccess returned [false] when the expected value was [true].");
            Assert.False(parsedValue);
        }

        [Fact]
        public void Bool_False_Should_Convert_To_False()
        {
            //-------------------------------------------------------------------------------------
            // NOTES: Testing whether the string to boolean converter can handle converting the
            //        string value "False" to it's boolean counterpart.
            //-------------------------------------------------------------------------------------

            // ARRANGE
            var value = "False";

            // ACT
            var isCastSuccess = value.TryConvertTo(out bool parsedValue);

            // ASSERT
            Assert.True(isCastSuccess, "The isCastSuccess returned [false] when the expected value was [true].");
            Assert.False(parsedValue);
        }

        [Fact]
        public void True_With_White_Space_Should_Be_Properly_Converted()
        {
            //-------------------------------------------------------------------------------------
            // NOTES: Testing whether the string to boolean converter can handle converting the
            //        string value "True" to it's boolean counterpart when there is white space.
            //-------------------------------------------------------------------------------------

            // ARRANGE
            var value = "                        true                                ";

            // ACT
            var isCastSuccess = value.TryConvertTo(out bool parsedValue);

            // ASSERT
            Assert.True(isCastSuccess, "The isCastSuccess returned [false] when the expected value was [true].");
            Assert.True(parsedValue);
        }

        [Fact]
        public void False_With_White_Space_Should_Be_Properly_Converted()
        {
            //-------------------------------------------------------------------------------------
            // NOTES: Testing whether the string to boolean converter can handle converting the
            //        string value "False" to it's boolean counterpart when there is white space.
            //-------------------------------------------------------------------------------------

            // ARRANGE
            var value = "                        false                                ";

            // ACT
            var isCastSuccess = value.TryConvertTo(out bool parsedValue);

            // ASSERT
            Assert.True(isCastSuccess, "The isCastSuccess returned [false] when the expected value was [true].");
            Assert.False(parsedValue);
        }

        [Fact]
        public void Null_Strings_Should_Return_False()
        {
            //-------------------------------------------------------------------------------------
            // NOTES: Testing whether the string to boolean converter will fail conversion and
            //        return false when the string is null.
            //-------------------------------------------------------------------------------------

            // ARRANGE
            string value = null;

            // ACT
            var isCastSuccess = value.TryConvertTo(out bool parsedValue);

            // ASSERT
            Assert.False(isCastSuccess, "The isCastSuccess returned [true] when the expected value was [false].");
            Assert.False(parsedValue);
        }

        [Fact]
        public void Non_Boolean_Strings_Should_Fail_Conversion()
        {
            //-------------------------------------------------------------------------------------
            // NOTES: When a string is not a valid boolean value, the converter should properly
            //        fail conversion.
            //-------------------------------------------------------------------------------------

            // ARRANGE
            string value = "This should not convert!";

            // ACT
            var isCastSuccess = value.TryConvertTo(out bool parsedValue);

            // ASSERT
            Assert.False(isCastSuccess, "The isCastSuccess returned [true] when the expected value was [false].");
            Assert.False(parsedValue);
        }
    }
}
