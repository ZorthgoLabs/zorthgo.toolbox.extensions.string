using System;
using Xunit;

namespace Zorthgo.Toolbox.Extensions.String.Test
{
    public class TryConvertToGenericsExtensionTests
    {
        [Fact]
        public void Convert_DateTime_Strings_To_Nullable_Types_Generic()
        {
            //-------------------------------------------------------------------------------------
            // NOTES: This test ensures that the type converter can convert Nullable types.
            //-------------------------------------------------------------------------------------

            // ARRANGE
            var value = "2016-03-04 18:36:22";
            DateTime.TryParse(value, out DateTime expectedParsedValue);

            // ACT
            var isCastSuccess = value.TryConvertTo(out DateTime? parsedValue);

            // ASSERT
            Assert.True(isCastSuccess, "The isCastSuccess value for casting of DateTime? types indicates a failure [Expected: True, Actual: False].");
            Assert.Equal(expectedParsedValue, parsedValue);
        }

        [Fact]
        public void Should_Return_Default_Is_Conversion_Fails()
        {
            //-------------------------------------------------------------------------------------
            // NOTES: When the conversion fails, the default for the specified type should be 
            //        returned.
            //-------------------------------------------------------------------------------------

            // ARRANGE
            var value = "THIS SHOULD NOT WORK!";

            // ACT
            var isCastSuccess = value.TryConvertTo(out int parsedValue);

            // ASSERT
            Assert.False(isCastSuccess, "The isCastSuccess returned [true] when the expected value was [false].");
            Assert.Equal(default, parsedValue);
        }

    }
}
