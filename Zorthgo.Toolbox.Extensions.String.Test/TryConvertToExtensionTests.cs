using System;
using Xunit;
using Zorthgo.Toolbox.Extensions.String.Test.Models;

namespace Zorthgo.Toolbox.Extensions.String.Test
{
    public class TryConvertToExtensionTests
    {
        [Fact]
        public void Convert_DateTime_Strings_To_Nullable_Types()
        {
            //-------------------------------------------------------------------------------------
            // NOTES: This test ensures that the type converter can convert Nullable types.
            //-------------------------------------------------------------------------------------

            // ARRANGE
            var value = "2016-03-04 18:36:22";
            var tType = typeof(DateTime?);
            DateTime.TryParse(value, out DateTime expectedParsedValue);

            // ACT
            var isCastSuccess = value.TryConvertTo(tType, out object parsedValue);

            // ASSERT
            Assert.True(isCastSuccess, "The isCastSuccess value for casting of DateTime? types indicates a failure [Expected: True, Actual: False].");
            Assert.Equal(expectedParsedValue, (DateTime)parsedValue);
        }

        [Fact]
        public void Convert_Empty_String_To_Nullable_Type()
        {
            //-------------------------------------------------------------------------------------
            // NOTES: This test ensures that the type converter can convert Nullable types when the
            //        string is an empty string.
            //-------------------------------------------------------------------------------------

            // ARRANGE
            var value = "";
            var tType = typeof(DateTime?);


            // ACT
            var isCastSuccess = value.TryConvertTo(tType, out object parsedValue);

            // ASSERT
            Assert.True(isCastSuccess, "The isCastSuccess value for casting of DateTime? types indicates a failure [Expected: True, Actual: False].");
            Assert.True((DateTime?)parsedValue == null);
        }

        [Fact]
        public void Convert_NULL_String_To_Nullable_Type()
        {
            //-------------------------------------------------------------------------------------
            // NOTES: This test ensures that the type converter can convert Nullable types when the
            //        string is an empty string.
            //-------------------------------------------------------------------------------------

            // ARRANGE
            var value = "NULL";
            var tType = typeof(DateTime?);


            // ACT
            var isCastSuccess = value.TryConvertTo(tType, out object parsedValue);

            // ASSERT
            Assert.True(isCastSuccess, "The isCastSuccess value for casting of DateTime? types indicates a failure [Expected: True, Actual: False].");
            Assert.True((DateTime?)parsedValue == null);
        }

        [Fact]
        public void Convert_String_Should_Return_A_String()
        {
            //-------------------------------------------------------------------------------------
            // NOTES: When we try to convert a string to a string, the method should simply return
            //        the same string.
            //-------------------------------------------------------------------------------------

            // ARRANGE
            var value = "This is a test.";

            // ACT
            var isCastSuccess = value.TryConvertTo(typeof(string), out object parsedValue);

            // ASSERT
            Assert.True(isCastSuccess, "The isCastSuccess value for casting of string to string indicates a failure [Expected: True, Actual: False].");
            Assert.True(value.Equals((string)parsedValue, StringComparison.InvariantCultureIgnoreCase));
        }

        [Fact]
        public void Should_Return_False_If_Type_Does_Not_Support_TryParse()
        {
            //-------------------------------------------------------------------------------------
            // NOTES: When we try to convert a string to a string, the method should simply return
            //        the same string.
            //-------------------------------------------------------------------------------------

            // ARRANGE
            var value = "This is a test.";

            // ACT
            var isCastSuccess = value.TryConvertTo(typeof(RandomClass), out object parsedValue);

            // ASSERT
            Assert.False(isCastSuccess, "The attempt to cast a string to a RandomClass type succeeded when the expected behaviour was for it to fail since the RandomClass doesn't support TryParse.");
            Assert.True(parsedValue == default(RandomClass));
        }

        [Fact]
        public void Should_Convert_Boolean()
        {
            //-------------------------------------------------------------------------------------
            // NOTES: Testing whether the string to boolean converter can handle converting "1" to 
            //        true.
            //-------------------------------------------------------------------------------------

            // ARRANGE
            var value = "1";

            // ACT
            var isCastSuccess = value.TryConvertTo(typeof(bool), out object parsedValue);

            // ASSERT
            Assert.True(isCastSuccess, "The isCastSuccess returned [false] when the expected value was [true].");
            Assert.True((bool)parsedValue);
        }
    }
}
