﻿using System;

namespace Zorthgo.Toolbox.Extensions.String
{
    // TODO: Move this extension method to the Zorthgo.Toolbox.Extensions.Boolean NuGet package.

    internal static class StringToBooleanConversion
    {
        // Represents the values that the TryConvertTo will handle for bool.
        private const string TrueLiteral = "True";
        private const string FalseLiteral = "False";
        private const string OneLiteral = "1";
        private const string ZeroLiteral = "0";

        /// <summary>
        /// This version of the Boolean TryConvertTo method handles (True | False | 1 | 0).
        /// </summary>
        /// <param name="value">The string value to be converted.</param>
        /// <param name="result">The converted bool.</param>
        /// <returns>Whether or not the method was able to convert the value.</returns>
        public static bool TryConvertTo(this string value, out bool result)
        {
            // Set the default value for result.
            result = false;

            // If string is null we cannot convert it.
            if (value == null)
                return false;

            // For performance reasons we will first try to convert the value without doing any trimming.
            if (TrueLiteral.Equals(value, StringComparison.OrdinalIgnoreCase) || OneLiteral.Equals(value, StringComparison.OrdinalIgnoreCase))
                return result = true;

            if (FalseLiteral.Equals(value, StringComparison.OrdinalIgnoreCase) || ZeroLiteral.Equals(value, StringComparison.OrdinalIgnoreCase))
                return true;


            // If we weren't able to convert it with the original string, we will try to trim it and see if we can convert afterwards..
            value = TrimWhiteSpaceAndNull(value);

            // Now that the string is trimmed, we try to convert it again.
            if (TrueLiteral.Equals(value, StringComparison.OrdinalIgnoreCase) || OneLiteral.Equals(value, StringComparison.OrdinalIgnoreCase))
                return result = true; 
            
            if (FalseLiteral.Equals(value, StringComparison.OrdinalIgnoreCase) || ZeroLiteral.Equals(value, StringComparison.OrdinalIgnoreCase))
                return true;
            
            return false;
        }

        /// <summary>
        /// Trims any white spaces or nulls from the provided string.
        /// </summary>
        /// <param name="value">The string to be trimmed.</param>
        /// <returns>The trimmed string.</returns>
        private static string TrimWhiteSpaceAndNull(string value)
        {
            var start = 0;
            var end = value.Length - 1;
            const char nullChar = (char)0x0000;

            while (start < value.Length)
            {
                if (!char.IsWhiteSpace(value[start]) && value[start] != nullChar)
                {
                    break;
                }
                start++;
            }

            while (end >= start)
            {
                if (!char.IsWhiteSpace(value[end]) && value[end] != nullChar)
                {
                    break;
                }
                end--;
            }

            return value.Substring(start, end - start + 1);
        }
    }
}
