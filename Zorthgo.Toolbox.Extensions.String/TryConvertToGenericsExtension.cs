﻿namespace Zorthgo.Toolbox.Extensions.String
{
    public static class TryConvertToGenericsExtension
    {
        /// <summary>
        /// Tries to convert the string value to the specified T type.
        /// </summary>
        /// <typeparam name="T">The type to which the string will be converted.</typeparam>
        /// <param name="value">The string value to be converted.</param>
        /// <param name="convertedValue">The converted T type object.</param>
        /// <returns></returns>
        public static bool TryConvertTo<T>(this string value, out T convertedValue)
        {
            // Calls the non-generic TryParse method.
            if (value.TryConvertTo(typeof(T), out object convValue))
            {
                // If conversion is successful, it returns the value. 
                convertedValue = (T)convValue;
                return true;
            }
            else
            {
                // If conversion failed, it return the default value for the T type.
                convertedValue = default;
                return false;
            }
        }
    }
}
