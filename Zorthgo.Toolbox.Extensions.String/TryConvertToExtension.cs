﻿using System;

namespace Zorthgo.Toolbox.Extensions.String
{
    public static class TryConvertToExtension
    {
        /// <summary>
        /// Tries to convert the specified string to the requested type.
        /// </summary>
        /// <param name="stringValue">String value.</param>
        /// <param name="targetType">Desired converted type.</param>
        /// <param name="convertedValue">Converted value in the new type.</param>
        /// <returns></returns>
        public static bool TryConvertTo(this string stringValue, Type targetType, out object convertedValue)
        {
            // If target type is a string then we don't need to do any conversion.
            if (targetType == typeof(string))
            {
                convertedValue = stringValue;
                return true;
            }

            // Handles Nullable<> types.
            if (targetType.IsGenericType && targetType.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                // If it's a nullable type and the value is either empty, or a NULL we return null.
                if (string.IsNullOrWhiteSpace(stringValue) || string.Equals(stringValue.Trim(), "null", StringComparison.InvariantCultureIgnoreCase))
                {
                    convertedValue = null;
                    return true;
                }

                // If the value is not an empty string or a NULL we set the underlying type so that we can TryParse the value.
                targetType = Nullable.GetUnderlyingType(targetType);
            }

            // Since we want to support 1 | 0 as valid booleans, we'll handle them separately.
            if (targetType == typeof(bool) && stringValue.TryConvertTo(out var result))
            {
                convertedValue = result;
                return true;
            }

            // Checks whether the type has the TryParse method.
            Type[] argTypes = { typeof(string), targetType.MakeByRefType() };
            var tryParseMethodInfo = targetType.GetMethod("TryParse", argTypes);
            if (tryParseMethodInfo == null)
            {
                convertedValue = null;
                return false;
            }

            // Calls the try parse method, and if the response is "false" it will return null as an out value.
            object[] args = { stringValue, null };
            var successfulParse = (bool)tryParseMethodInfo.Invoke(null, args);
            if (!successfulParse)
            {
                convertedValue = null;
                return false;
            }

            // If the conversion was successful, then we return the converted value.
            convertedValue = args[1];
            return true;
        }
    }
}
